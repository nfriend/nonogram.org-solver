const topCluesEl = document.querySelector('.nmtt');
const leftCluesEl = document.querySelector('.nmtl');
const gridEl = document.querySelector('.nmtc');

function rgbStringToHex(rgbString: string) {
  const parser = /^rgb\(([0-9]+), ([0-9]+), ([0-9]+)\)/g;
  const matcher = parser.exec(rgbString);

  function componentToHex(c) {
    var hex = c.toString(16);
    return hex.length == 1 ? '0' + hex : hex;
  }

  function rgbToHex(r, g, b) {
    return '#' + componentToHex(r) + componentToHex(g) + componentToHex(b);
  }

  return rgbToHex(
    parseInt(matcher[1], 10),
    parseInt(matcher[2], 10),
    parseInt(matcher[3], 10),
  );
}

const gridDimensions = {
  width: gridEl.querySelectorAll('tr:first-child>td').length,
  height: gridEl.querySelectorAll('tr').length,
};

interface Clue {
  color: string;
  length: number;
}

const clues: {
  top: Clue[][];
  left: Clue[][];
} = {
  top: [],
  left: [],
};

// populate the clues from the top grid
let rows = Array.from(topCluesEl.querySelectorAll('tr'));
rows.reverse().forEach(row => {
  const cells = Array.from(row.querySelectorAll('td'));
  cells.forEach((c, cellIndex) => {
    const cellNum = parseInt(c.innerText, 10);

    if (!isNaN(cellNum)) {
      if (!clues.top[cellIndex]) {
        clues.top[cellIndex] = [];
      }
      clues.top[cellIndex].push({
        color: rgbStringToHex(
          getComputedStyle(c).getPropertyValue('background-color'),
        ),
        length: cellNum,
      });
    }
  });
});

// populate the clues from the left grid
rows = Array.from(leftCluesEl.querySelectorAll('tr'));
rows.forEach((row, rowIndex) => {
  const cells = Array.from(row.querySelectorAll('td'));
  cells.reverse().forEach((c, cellIndex) => {
    const cellNum = parseInt(c.innerText, 10);

    if (!isNaN(cellNum)) {
      if (!clues.left[rowIndex]) {
        clues.left[rowIndex] = [];
      }
      clues.left[rowIndex].push({
        color: rgbStringToHex(
          getComputedStyle(c).getPropertyValue('background-color'),
        ),
        length: cellNum,
      });
    }
  });
});

console.log('clues:', clues);
